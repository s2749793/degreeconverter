package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 * Tests the Quoter
 */
public class TestQuoter {

    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10.0d,price,0.0d,"Price of book 1");
    }
}
