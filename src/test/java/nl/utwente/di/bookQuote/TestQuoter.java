package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 * Tests the Quoter
 */
public class TestQuoter {

    @Test
    public void testConversion() throws Exception {
        Converter converter = new Converter();
        float temp = converter.getCelciusToFarenheit("10");
        Assertions.assertEquals(50f,temp,0.0d,"temp in fahrenheit");
    }
}
