package nl.utwente.di.bookQuote;

import java.util.Map;

public class Converter {

    public float getCelciusToFarenheit(String celcius) {
        try {
            return Float.parseFloat(celcius)*1.8f + 32f;
        } catch (Exception e) {
            return Float.NaN;
        }
    }
}
