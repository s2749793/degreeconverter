package java.nl.utwente.di.bookQuote;

import nl.utwente.di.bookQuote.Converter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 * Tests the Quoter
 */
public class TestQuoter {

    @Test
    public void testConversion() throws Exception {
        Converter quoter = new Converter();
        float fahrenheit = quoter.getCelciusToFarenheit("10");
        Assertions.assertEquals(10f,fahrenheit,0.0f,"degree conversion");
    }
}
